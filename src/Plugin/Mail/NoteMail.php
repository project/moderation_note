<?php

namespace Drupal\moderation_note\Plugin\Mail;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Moderation Note mail backend.
 *
 * @Mail(
 *   id = "moderation_note",
 *   label = @Translation("Moderation note mailer"),
 *   description = @Translation("Sends HTML emails for Moderation Note.")
 * )
 */
final class NoteMail extends PhpMail implements ContainerFactoryPluginInterface {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new NoteMail instance.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(StateInterface $state, ConfigFactoryInterface $config_factory) {
    $this->state = $state;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $container->get('state'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    $message['body'] = implode("\n\n", $message['body']);
    // We have to override the parent method so that HTML is not escaped.
    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed; delsp=yes';
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {
    // We're running inside a functional test.
    $config = $this->configFactory->get('system.mail');
    if ($config->get('interface.default') === 'test_mail_collector') {
      $captured_emails = $this->state->get('system.test_mail_collector') ?: [];
      $captured_emails[] = $message;
      $this->state->set('system.test_mail_collector', $captured_emails);
      return TRUE;
    }

    return parent::mail($message);
  }

}
