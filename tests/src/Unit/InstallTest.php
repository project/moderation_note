<?php

declare(strict_types=1);

namespace Drupal\Tests\moderation_note\Unit;

use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Tests the moderation_note module's installation.
 */
class InstallTest extends UnitTestCase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();

    $this->configFactory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');
    $container->set('config.factory', $this->configFactory);

    \Drupal::setContainer($container);
    require_once __DIR__ . '/../../../moderation_note.install';

  }

  /**
   * Tests that the moderation_note module is installed.
   *
   * @covers ::moderation_note_install
   */
  public function testHookInstall() {

    $config = $this->createMock('Drupal\Core\Config\Config');
    $config->expects($this->once())
      ->method('set')
      ->with('interface', ['moderation_note' => 'moderation_note'])
      ->willReturnSelf();
    $config->expects($this->once())
      ->method('save');

    $this->configFactory->expects($this->once())
      ->method('getEditable')
      ->with('system.mail')
      ->willReturn($config);

    moderation_note_install();
  }

}
